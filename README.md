## Local test environments using Docker Desktop

This project configures local GitLab Docker Containers, for different versions of GitLab, under a common Docker Project. This is to have quick and easy installs of different GitLab versions for your local test environment.

It relies upon [Docker Desktop for Mac](https://www.docker.com/get-started), using its built-in hypervisor for the Containers, and [GitLab's Omnibus Docker Images](https://docs.gitlab.com/omnibus/docker/README.html).  There is no need for a Linux virtual machine running in VirtualBox or VMWare.  If you _are_ using Linux to host your Docker Containers, you'll probably have more fun with [Docker Engine](https://docs-stage.docker.com/engine/install/). See [GitLab's Docker Images documentation](https://docs.gitlab.com/omnibus/docker/README.html) for some help.

The Containers are set up using [Docker Compose](https://docs.docker.com/compose/). 
All configuration is in `docker-compose.yml`.  You can add separate `services` for each GitLab instance that you want to maintain.

 * The Docker Containers will mount host filesystems for GitLab's `logs`, `data`, and `config` (settings) in directories named `gitlab-nn` where _nn_ is the major version number
 * The Container's TCP ports are mapped back to the host as-is:  Host port :80 goes to the Container's HTTP listener, Host :22 to the Container's SSH
 * In the Docker Desktop Dashboard, all GitLabs will be grouped under the `gitlab-docker` Project (actually named by the directory you cloned to)
 * All containers use the same domain name: `gitlab-docker.test`. I consider this less hassle than updating your `/etc/hosts` for new containers

 ### Known Limitations

 * Single-Container [GitLab Omnibus Docker images](https://docs.gitlab.com/omnibus/docker/README.html) only
 * Starting multiple GitLabs at once is _possible_, ***but not recommended***: 
   - Performance is significantly limiting: you will get a lot of missed deadline errors
   - You will need to change the TCP port mapping in _interesting_ ways to prevent clashes&hellip;
   - Changing TCP ports breaks some links in the UI, and changing SSH will make it so that you cannot `git clone git@gitlab-docker.test:`&hellip;
 * TLS (SSL) with [LetsEncrypt](https://docs.gitlab.com/omnibus/settings/ssl.html#lets-encrypt-fails-on-reconfigure) is _not_ supported

### Caveats
 * Having more than one GitLab with the same _Major_ version will require to include the _Minor_ version number in the `container_name` and `volumes` paths, see the discussion below
 * Git cloning with HTTPS (from the test service) hasn't been considered

---

> Have a look at the Support Group's [docker-compose toolbox project](https://gitlab.com/gitlab-com/support/toolbox/docker-compose) &mdash; it has intergrations using mulitple containers!

---

## Using this project

 1. Clone the repository (Fork it first, if you want)
 1. Add an alias to your Mac's `/etc/hosts` file (you need just one)
 ```
 127.0.0.1        gitlab-docker.test
 ```
 3. Make Sure **Remote Login** is disabled on your Mac (Apple menu > System Preferences > Sharing > Remote Login is _not_ checked). This allows SSH into your Mac's loopback to be served by the Container
 1. Start a single GitLab Container (where _nn_ is the GitLab service (major version) defined in `docker-compose.yml`):
 ```
 docker-compose up -d gitlab-nn
 ```
 5. Watch the logs, either through the host-mounts in `./gitlab-nn/logs/`, or with the Docker Desktop
 1. Once Chef has finished building the GitLab Omnibus, browse to `http://gitlab-docker.test` to see the GitLab UI
 1. You can connect to a shell within the Container using Docker's facilities: `docker exec -it gitlab-nn bash`
 1. After creating a project with a repository and adding you SSH key, you can clone it normally, using the URL provided by GitLab's **Clone** button
 1. Use `docker-compose down gitlab-nn` to stop the Container once you're done with it
 1. Use `docker-compose rm gitlab-nn' to remove the Container and reclaim some space. The volumes remain, use `-v` to clean those up as well
 1. use 'docker image ls' and 'docker image rm' or 'prune' to manage GitLab Docker Images


### Maintaining _Permanent_ GitLab containers

 If you **stop** the Container, it will be rebuilt the next time that you start it!
 For persistent containers, use `docker pause gitlab-nn` and `docker unpause gitlab-nn`

 ---
 > CARE:  running `docker-compose up ...` on a paused container will recreate it!
 ---

## Adding more GitLabs to your Project

Just copy an entire `gitlab-`_nn_ `service` block and change the numbers. You can usually just search/replace "`gitlab-12`":

```yaml
  gitlab-12:
    container_name: gitlab-12
    image: gitlab/gitlab-ee:12.4.2-ee.0
    restart: unless-stopped
    hostname: 'gitlab-docker.test'
    environment:
      GITLAB_OMNIBUS_CONFIG: |
        external_url 'http://gitlab-docker.test'
        letsencrypt['enabled'] = false
    ports:
      - '80:80'
      - '443:443'
      - '22:22'
    volumes:
      - './gitlab-12/config:/etc/gitlab'
      - './gitlab-12/logs:/var/log/gitlab'
      - './gitlab-12/data:/var/opt/gitlab'
```

You will need to specify an `image`. To see a list of GitLab's available Docker Images on DockerHub, run this command:

```
wget -q https://registry.hub.docker.com/v1/repositories/gitlab/gitlab-ee/tags -O - | sed -e 's/[][]//g' -e 's/"//g' -e 's/ //g' | tr '}' '\n' | awk -F: '{print $3}'
```

(this could probably be made into a nicer `jq` command, but then you'd have to install [jq](https://stedolan.github.io/jq/))


## Multiple GitLabs with the same _Major_/_Minor_ version

If you need to have more than one GitLab of the same _major_ version (both GitLab 11.8 and GitLab 11.10, say) then extend the numbering to include the _minor_ version number as well (e.g. for the Service names, and the Volume paths: `gitlab-nn.v` where _nn_ is the _major_ version, and _v_ is the _minor_ version).

For _patch_ releases (e.g. 13.2.0 _and_ 13.2.2), you can extend you naming and numbering even more: `gitlab-nn.v.x`. Keep the `container_name` and the beginning of the paths in `volumes` consistent:

 - This makes it easy to locate the right volume
 - You can use shell TAB-completion for your `docker-compose up -d` (or `pause`/`unpause`) because the volume directory names are the same as the container names

